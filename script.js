$(function(){
		if(localStorage.operations==undefined){
		localStorage.operations = "";
	}
		$("#cash-input").keyup(function(event) {
			if (event.keyCode === 13) {
	        	$("#submit").click();
		    }
		});
		//Initialisation de la page
		$('#title').html('Réaliser un dépot');
					$('#operation').css('display','block');
					$('#history').css('display','none');
					type = "deposit";
		//Au choix du menu
		$('#choice li').click(function(e){
			setPage(e);
		});

		//set the correct page of the selection
		function setPage(e){
			switch(e.currentTarget.innerText){
				case "DEPOT":
					$('#title').html('Réaliser un dépot');
					$('#operation').css('display','block');
					$('#history').css('display','none');
					type = "deposit";
				break;
				case "RETRAIT":
					$('#title').html('Réaliser un retrait');
					$('#operation').css('display','block');
					$('#history').css('display','none');
					type = "withdrawal";
				break;
				case "HISTORIQUE":
					type = "history";
					$('#title').html('Historique des transactions');
					$('#operation').css('display','none');
					$('#history').css('display','block');
					//build the page history
					setHistoryPage();
				break;
			}
		}

		//build the page history
		function setHistoryPage(){
			if(localStorage.operations!=""){
				$('#history h2').css('display','none');
				$('#history .header, #history #history-content').css('display','block');
			var response = constructOperationsHistory();
			$('#amount').html(response.balance);
			$('#history-content table tbody').html(response.items);
			}else{
				$('#history h2').css('display','block');
				$('#history .header, #history #history-content').css('display','none');
			}
		}

		function constructOperationsHistory(){
			var operations = JSON.parse(localStorage.getItem('operations'));
			var history = {};
			history.balance = 0;
			history.items = "";
			for(var i = 0; i < operations.length; i++){
				var dateOfOperation = new Date(operations[i].date);
				var frenchDate = dateOfOperation.getDate() +"/"+dateOfOperation.getMonth()+"/"+dateOfOperation.getFullYear();
				var itemDiv = "<tr><td>"+frenchDate+"</td><td class='";
				if(operations[i].amount<0){
					itemDiv +="negative"
				}else{
					itemDiv +="positive"
				}
				itemDiv +="'>"+operations[i].amount+" €</td>";
				history.balance +=operations[i].amount;
				itemDiv +="<td>"+history.balance+" €</td></tr>";
				history.items +=itemDiv;
			}
			return history;
		}

		function calculateAmount(operations,arg1){
			var amount = 0;
					for(var i = 0; i < operations.length; i++){
						amount += operations[i].amount;
					}
					if(amount+arg1 >= 0){
						return true;
					}else{
						return false;
					}
		}



		$('#submit').click(function(e){
			$('#error').html('');
			if($('#cash-input').val()!="" && $('#cash-input').val() > 0){
				if(localStorage.operations!=""){
				var operations = JSON.parse(localStorage.getItem('operations'));
			}else{
				var operations = [];
			}
				var operation = {};
				operation.date = new Date();
				switch(type){
					case "deposit":
					operation.amount = $('#cash-input').val()*1
						operations.push(operation);
						localStorage.setItem('operations', JSON.stringify(operations));
						break;
					case "withdrawal":
					operation.amount = $('#cash-input').val()*(-1)
						if(calculateAmount(operations,$('#cash-input').val()*(-1))){
							operations.push(operation);
							localStorage.setItem('operations', JSON.stringify(operations));
						}else{
							$('#error').html('Montant du retrait trop élevé.');
						}
						break;
				}		
				
			}else{
				$('#error').html('Veuillez saisir un montant supérieur à 0');
			}
			$('#cash-input').val("");
		});
	
		
	});